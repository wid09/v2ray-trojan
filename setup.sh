#!/bin/sh
## Auto Script by Gugun

if [[ $EUID -ne 0 ]]; then
    clear
    echo "Error: This script must be run as root!" 1>&2
    exit 1
fi

timedatectl set-timezone Asia/Jakarta
v2uuid=$(cat /proc/sys/kernel/random/uuid)

# Disable IPv6
sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1
sysctl -w net.ipv6.conf.lo.disable_ipv6=1
echo -e "net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf

# Install DDoS Deflate
cd
apt install -y dnsutils tcpdump dsniff grepcidr
wget -qO ddos.zip "https://raw.githubusercontent.com/iriszz-official/autoscript/main/FILES/ddos-deflate.zip"
unzip ddos.zip
cd ddos-deflate
chmod +x install.sh
./install.sh
cd
rm -rf ddos.zip ddos-deflate

# Install fail2ban
apt install -y fail2ban
service fail2ban restart

install_ssl(){
    if [ -f "/usr/bin/apt-get" ];then
            isDebian=`cat /etc/issue|grep Debian`
            if [ "$isDebian" != "" ];then
                    apt install -y net-tools
                    sleep 3s
            else
                    apt install -y net-tools
                    sleep 3s
            fi
    else
        yum install -y epel-release
        yum install -y net-tools
        sleep 3s
    fi

    isPort=`netstat -ntlp| grep -E ':80 |:443 '`
    if [ "$isPort" != "" ];then
            clear
            echo " ================================================== "
            echo " Port 80 or 443 is occupied, please release the port before running this script"
            echo
            echo " Port occupancy information is as follows："
            echo $isPort
            echo " ================================================== "
            exit 1
    fi

read -p "Please enter your domain : " domain
    
    if [ -f "/usr/bin/apt-get" ];then
            isDebian=`cat /etc/issue|grep Debian`
            if [ "$isDebian" != "" ];then
                    apt install -y snapd
                    snap install core; snap refresh core
                    snap install --classic certbot
                    ln -s /snap/bin/certbot /usr/bin/certbot
                    echo "Y" | certbot certonly --renew-by-default --register-unsafely-without-email --standalone -d $domain
                    echo -e "0 2 1 * * /usr/bin/certbot renew --pre-hook \"service nginx stop\" --post-hook \"service nginx start\"" >> /var/spool/cron/crontabs/root
                    systemctl restart cron.service
                    sleep 3s
            else
                    apt install -y certbot
                    echo "A" | certbot certonly --renew-by-default --register-unsafely-without-email --standalone -d $domain
                    echo -e "0 2 1 * * /usr/bin/certbot renew --pre-hook \"service nginx stop\" --post-hook \"service nginx start\"" >> /var/spool/cron/crontabs/root
                    systemctl restart cron.service
                    sleep 3s
            fi
    else
        yum install -y certbot
        echo "Y" | certbot certonly --renew-by-default --register-unsafely-without-email --standalone -d $domain
        echo -e "0 2 1 * * /usr/bin/certbot renew --pre-hook \"service nginx stop\" --post-hook \"service nginx start\"" >> /var/spool/cron/root
        systemctl restart crond.service
        sleep 3s
    fi
}

install_nginx(){
        if [ -f "/usr/bin/apt-get" ];then
            isDebian=`cat /etc/issue|grep Debian`
            if [ "$isDebian" != "" ];then
                    apt install -y build-essential libtool libpcre3 libpcre3-dev zlib1g-dev openssl libssl-dev
                    sleep 3s
            else
                    apt install -y build-essential libtool libpcre3 libpcre3-dev zlib1g-dev openssl libssl-dev
                    sleep 3s
            fi
    else
        yum install -y gcc gcc-c++ zlib zlib-devel openssl openssl-devel pcre-devel
        sleep 3s
    fi

    wget https://nginx.org/download/nginx-1.21.1.tar.gz -O - | tar -xz
    cd nginx-1.21.1
    ./configure --prefix=/etc/nginx \
    --sbin-path=/usr/sbin/nginx \
    --modules-path=/usr/lib/nginx/modules \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --pid-path=/var/run/nginx.pid \
    --lock-path=/var/run/nginx.lock \
    --with-http_v2_module \
    --with-http_ssl_module \
    --with-http_gzip_static_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-stream \
    --with-stream_ssl_module
    
    make && make install
    cd ..
    rm -rf nginx-1.21.1
    
cat >/lib/systemd/system/nginx.service<<EOF
[Unit]
Description=The NGINX HTTP and reverse proxy server
After=syslog.target network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target
[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/usr/sbin/nginx -s reload
ExecStop=/bin/kill -s QUIT \$MAINPID
PrivateTmp=true
[Install]
WantedBy=multi-user.target
EOF

cat >/etc/nginx/nginx.conf<<EOF
pid /var/run/nginx.pid;
worker_processes auto;
worker_rlimit_nofile 51200;
events {
    worker_connections 1024;
    multi_accept on;
    use epoll;
}
http {
    server_tokens off;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 120s;
    keepalive_requests 10000;
    types_hash_max_size 2048;
    include /etc/nginx/mime.types;
    access_log off;
    error_log /dev/null;
    server {
        listen 89;
        listen [::]:89;
        server_name $domain;
        location / {
            return 301 https://\$server_name\$request_uri;
        }
    }
    server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name $domain;
        ssl_protocols TLSv1.1 TLSv1.2 TLSv1.3;
        ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:HIGH:!aNULL:!MD5:!RC4:!DHE;
        ssl_prefer_server_ciphers on;
        ssl_certificate /etc/letsencrypt/live/$domain/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;

        location = /trojan-ws {
            if (\$http_upgrade != "websocket") {
                return 404;
            }
            proxy_redirect off;
            proxy_pass http://unix:/dev/shm/tws.sock;
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host \$host;
            proxy_set_header X-Real-IP \$remote_addr;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        }

        location /trojan-grpc {
            if (\$request_method != "POST") {
                return 404;
            }
            client_body_buffer_size 1m;
            client_body_timeout 1h;
            client_max_body_size 0;
            grpc_pass grpc://unix:/dev/shm/tgrpc.sock;
            grpc_read_timeout 1h;
            grpc_send_timeout 1h;
            grpc_set_header X-Real-IP \$remote_addr;
        }

    }
}
EOF

    systemctl daemon-reload && systemctl enable nginx.service && systemctl start nginx.service
}

install_v2ray(){    
    wget https://raw.githubusercontent.com/v2fly/fhs-install-v2ray/master/install-release.sh && bash install-release.sh
#List akun Trojan
touch /usr/local/etc/v2ray/trojan.conf
cat >/usr/local/etc/v2ray/config.json<<EOF
{
  "log": {
    "loglevel": "warning",
    "error": "/var/log/v2ray/error.log", 
    "access": "/var/log/v2ray/access.log"
  },
  "inbounds": [
    {
      "listen": "/dev/shm/tws.sock",
      "protocol": "trojan",
      "settings": {
        "clients": [
          {
            "password":"gugun",
            "email": "gugun@gmail.com"
          }
        ]
      },
      "streamSettings": {
        "network": "ws",
        "security": "none",
        "wsSettings": {
          "path": "/trojan-ws"
        }
      },
      "sniffing": {
        "enabled": true,
        "destOverride": [
          "http",
          "tls"
        ]
      }
    },
    {
      "listen": "/dev/shm/tgrpc.sock",
      "protocol": "trojan",
      "settings": {
        "clients": [
          {
            "password":"gugun",
            "email": "gugun@gmail.com"
          }
        ]
      },
      "streamSettings": {
        "network": "grpc",
        "security": "none",
        "grpcSettings": {
          "serviceName": "trojan-grpc"
        }
      },
      "sniffing": {
        "enabled": true,
        "destOverride": [
          "http",
          "tls"
        ]
      }
    }
  ],
  "routing": {
    "rules": [
      {
        "type": "field",
        "protocol": [
          "bittorrent"
        ],
        "outboundTag": "blocked"
      }
    ]
  },
  "outbounds": [
    {
      "protocol": "freedom",
      "settings": {}
    },
    {
      "protocol": "blackhole",
      "settings": {},
      "tag": "blocked"
    }
  ],
  "routing": {
    "rules": [
      {
        "type": "field",
        "ip": [
          "0.0.0.0/8",
          "10.0.0.0/8",
          "100.64.0.0/10",
          "169.254.0.0/16",
          "172.16.0.0/12",
          "192.0.0.0/24",
          "192.0.2.0/24",
          "192.168.0.0/16",
          "198.18.0.0/15",
          "198.51.100.0/24",
          "203.0.113.0/24",
          "::1/128",
          "fc00::/7",
          "fe80::/10"
        ],
        "outboundTag": "blocked"
      },
      {
        "type": "field",
        "outboundTag": "blocked",
        "protocol": [
          "bittorrent"
        ]
      }
    ]
  }
}
EOF
    echo ${domain} > /usr/local/etc/v2ray/domain
    systemctl enable v2ray.service && systemctl start v2ray.service
    rm -f setup.sh install-release.sh
    clear
}

install_bbr(){
  echo "0 4 * * * root reboot" >> /etc/crontab
  echo "0 0 * * * root xp-ws" >> /etc/crontab
  wget https://gist.githubusercontent.com/hyperfact/4d024d66fb494727355fa5739ef75ed9/raw/4639a46fcc65b3b3cee6a8df1843ab03fdf71eb9/bbr.sh && chmod +x bbr.sh && ./bbr.sh
}

list_port(){
echo "   - Trojan WS      : 443"  | tee -a log-install.txt
echo "   - Trojan gRPC    : 443"  | tee -a log-install.txt

}

install_badvpn(){
  wget -O /usr/bin/badvpn-udpgw "https://github.com/Gugun09/AutoScriptVPS/raw/master/Files/BadVPN/badvpn-udpgw64"
  chmod +x /usr/bin/badvpn-udpgw
  sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7100 --max-clients 500' /etc/rc.local
  sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7200 --max-clients 500' /etc/rc.local
  sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 500' /etc/rc.local
  sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7400 --max-clients 500' /etc/rc.local
  sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7500 --max-clients 500' /etc/rc.local
  # sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7600 --max-clients 500' /etc/rc.local
  # sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7700 --max-clients 500' /etc/rc.local
  # sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7800 --max-clients 500' /etc/rc.local
  # sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7900 --max-clients 500' /etc/rc.local
  screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7100 --max-clients 500
  screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7200 --max-clients 500
  screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 500
  screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7400 --max-clients 500
  screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7500 --max-clients 500
  # screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7600 --max-clients 500
  # screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7700 --max-clients 500
  # screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7800 --max-clients 500
  # screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7900 --max-clients 500
}

list_menu(){
# Download Menu
cd /usr/bin
wget -O addws "https://gitlab.com/wid09/vip/-/raw/main/addws.sh" && chmod +x addws
wget -O dellws "https://gitlab.com/wid09/vip/-/raw/main/dellws.sh" && chmod +x dellws
wget -O xp-ws "https://gitlab.com/wid09/vip/-/raw/main/xp-ws.sh" && chmod +x xp-ws
}

start_menu(){
    clear
    echo " ================================================== "
    echo "             Auto Instal V2Ray WS                   "
    echo "       Support ：Ubuntu、Debian、CentOS              "
    echo " ================================================== "
    echo
    echo " 1. V2Ray+WS+gRPC+TLS"
    echo " 2. XRay+WS+gRPC+TLS"
    echo " 0. Exit Script"
    echo
    read -p "Please enter the number:" num
    case "$num" in
    1)
    install_ssl
    install_nginx
    install_v2ray
    install_bbr
    list_port
    install_badvpn
    # list_menu
    ;;
    2)
    echo "Comingsoon..."
    sleep 2s
    start_menu
    ;;
    0)
    exit 1
    ;;
    *)
    clear
    echo "Please enter the correct number"
    sleep 2s
    start_menu
    ;;
    esac
}

start_menu

